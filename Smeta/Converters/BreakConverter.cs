﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Smeta.Converters
{
    class BreakConverter : IValueConverter
    {
            public object Convert(object value, Type targetType,
                object parameter, CultureInfo culture)
            {
                Debugger.Break();
                return value;
            }

            public object ConvertBack(object value, Type targetType,
                object parameter, CultureInfo culture)
            {
                Debugger.Break();
                return value;
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            Debugger.Break();
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            Debugger.Break();
            return value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Smeta.Converters
{
    class DecimalString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            if (targetType != typeof(string))
                throw new ArgumentException("Wrong target type!");

            if (value.GetType() != typeof(decimal))
                throw new ArgumentException("Wrong source type!");

            var dec = (decimal)value;
            if (dec == 0m)
                return "";
            return String.Format("{0}", dec);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, string culture)
        {
            if (targetType != typeof(decimal))
                throw new ArgumentException("Wrong target type!");

            if (value.GetType() != typeof(string))
                throw new ArgumentException("Wrong source type!");

            var str = value as string;
            if (string.IsNullOrWhiteSpace(str))
                return 0m;
            var done = decimal.TryParse(str, out var result);
            if (done)
            {
                return result;
            }
            else
            {
                return 0m;
            }
        }
    }
}

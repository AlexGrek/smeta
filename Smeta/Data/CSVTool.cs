﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Smeta.Models;
using Windows.Storage;

namespace Smeta.Data
{
    public class CSVTool : IDataProvider
    {
        public StorageFile CurrentFile { get; private set; }

        private async Task DumpTo(StorageFile f, Estimate estimate)
        {
            var xml = new XmlSerializer(typeof(SerializationModels.SerializationEstimate));
            var buffer = new StringBuilder();
            foreach (var table in Serialize(estimate))
            {
                foreach (var row in table)
                {
                    buffer.AppendLine(row);
                }
            }
            await FileIO.WriteTextAsync(f, buffer.ToString());
        }

        public IEnumerable<string> Serialize(Table table)
        {
            foreach (var row in table.Rows)
            {
                yield return $"{row.ID},{row.Name},{row.Units},{row.Price},{row.Count},{row.Sum}";
            }
        }

        public IEnumerable<IEnumerable<string>> Serialize(Estimate est)
        {
            foreach (var table in est.Tables)
            {
                yield return Serialize(table);
            }
        }

        public async Task<Estimate> LoadFrom(StorageFile f)
        {
            throw new NotImplementedException();
        }

        public CSVTool(StorageFile f)
        {
            CurrentFile = f;
        }

        public async Task<Estimate> CreateAsync(string name)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteByNameAsync(string name)
        {
            throw new NotImplementedException();
        }

        public async Task<Estimate> ReadByNameAsync(string name)
        {
            throw new NotImplementedException();
        }

        public async Task<Estimate> UpdateByNameAsync(string name, Estimate est)
        {
            await DumpTo(CurrentFile, est);
            return est;
        }
    }
}

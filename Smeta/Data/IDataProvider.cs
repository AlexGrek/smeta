﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smeta.Models;

namespace Smeta.Data
{
    public interface IDataProvider
    {
        Task<Estimate> ReadByNameAsync(string name);

        Task<Estimate> UpdateByNameAsync(string name, Estimate est);

        Task DeleteByNameAsync(string name);

        Task<Estimate> CreateAsync(string name);
    }
}

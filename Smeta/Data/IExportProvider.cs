﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smeta.Models;

namespace Smeta.Data
{
    public interface IExportProvider
    {
        void Export(string path, Estimate estimate);
    }
}

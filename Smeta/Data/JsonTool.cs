﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smeta.Models;
using System.IO;
using Windows.Storage;
using Newtonsoft.Json;

namespace Smeta.Data
{
    public class JsonTool : IDataProvider
    {
        StorageFolder _localStorage = ApplicationData.Current.LocalFolder;

        public StorageFile CurrentFile { get; private set; }

        public JsonTool(StorageFile f)
        {
            CurrentFile = f;
        }

        public string AbsPath(string name)
        {
            return Path.Combine(_localStorage.Path, name + ".json");
        }

        private async Task DumpTo(StorageFile f, Estimate estimate)
        {
            string json = JsonConvert.SerializeObject(estimate);
            await FileIO.WriteTextAsync(f, json);
        }

        public async Task<Estimate> CreateAsync(string name)
        {
            var est = new Estimate();
            await DumpTo(CurrentFile, est);
            return est;
        }

        public async Task DeleteByNameAsync(string name)
        {
            await CurrentFile.DeleteAsync(StorageDeleteOption.Default);
        }

        public async Task<Estimate> ReadByNameAsync(string name)
        {
            var text = File.ReadAllText(AbsPath(name));
            return JsonConvert.DeserializeObject<Estimate>(text);
        }

        public async Task<Estimate> UpdateByNameAsync(string name, Estimate est)
        {
            await DumpTo(CurrentFile, est);
            return est;
        }
    }
}

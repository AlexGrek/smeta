﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Smeta.Models;
using Windows.Storage;

namespace Smeta.Data
{
    public class XMLTool : IDataProvider
    {
        public StorageFile CurrentFile { get; private set; }

        private async Task DumpTo(StorageFile f, Estimate estimate)
        {
            var xml = new XmlSerializer(typeof(SerializationModels.SerializationEstimate));
            using (var stream = await f.OpenStreamForWriteAsync())
            {
                var item = SerializationModels.SerializationEstimate.Create(estimate);
                xml.Serialize(stream, item);
            }
        }

        public async Task<Estimate> LoadFrom(StorageFile f)
        {
            var xml = new XmlSerializer(typeof(SerializationModels.SerializationEstimate));
            using (var stream = await f.OpenStreamForReadAsync())
            {
                var obj = await Task.Run(() => xml.Deserialize(stream));
                var estmodel = obj as SerializationModels.SerializationEstimate;
                if (estmodel == null)
                {
                    throw new FileLoadException("Wrong deserialization type");
                }
                return estmodel.Decode();
            }
        }

        public XMLTool(StorageFile f)
        {
            CurrentFile = f;
        }

        public async Task<Estimate> CreateAsync(string name)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteByNameAsync(string name)
        {
            throw new NotImplementedException();
        }

        public async Task<Estimate> ReadByNameAsync(string name)
        {
            throw new NotImplementedException();
        }

        public async Task<Estimate> UpdateByNameAsync(string name, Estimate est)
        {
            await DumpTo(CurrentFile, est);
            return est;
        }
    }
}

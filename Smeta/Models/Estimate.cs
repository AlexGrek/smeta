﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Smeta.Models
{
    public class Estimate
    {
        private List<Table> _tables;
        string _name = "";
        decimal _sum;

        public static Estimate CreateInitial()
        {
            var e = new Estimate();
            e.Name = "Estimate";
            e.Add(Table.CreateInitial());
            return e;
        }

        /// <summary>
        /// Triggers when sum is recalculated
        /// </summary>
        public event SumChanged OnSumChanged;

        public List<Table> Tables { get => _tables; private set => _tables = value; }

        public string Name { get => _name; set => _name = value; }
        public decimal Sum { get => _sum; private set => _sum = value; }

        public Estimate()
        {
            Tables = new List<Table>();
        }

        public Estimate(string name, IEnumerable<Table> tables = null): this()
        {
            _name = name;

            if (tables != null)
            {
                Tables.AddRange(tables);
            }
        }

        public decimal CalculateSum()
        {
            var newsum = Tables.Sum(table => table.Sum);
            var oldsum = Sum;
            Sum = newsum;
            OnSumChanged?.Invoke(newsum, oldsum);
            return newsum;
        }

        /// <summary>
        /// Implementation of table.SumChanged
        /// </summary>
        /// <param name="news">new sum</param>
        /// <param name="olds">old sum</param>
        private void Table_OnSumChanged(decimal news, decimal olds)
        {
            CalculateSum();
        }

        #region CRUD

        public Table AddEmpty()
        {
            var table = new Table();
            table.OnSumChanged += Table_OnSumChanged;
            CalculateSum();
            return table;
        }

        public void Add(Table t)
        {
            t.OnSumChanged += Table_OnSumChanged;
            Tables.Add(t);
            CalculateSum();
        }

        public void AddRange(IEnumerable<Table> tables)
        {
            foreach (var t in tables)
            {
                Add(t);
            }
            CalculateSum();
        }

        public void Insert(int a)
        {
            var row = new Table();
            row.OnSumChanged += Table_OnSumChanged;
            Tables.Insert(a, row);
            CalculateSum();
        }

        public void Insert(int a, Table t)
        {
            t.OnSumChanged += Table_OnSumChanged;
            Tables.Insert(a, t);
            CalculateSum();
        }

        public void Delete(int i)
        {
            var tabl = Tables[i];
            Tables.RemoveAt(i);
            tabl.OnSumChanged -= Table_OnSumChanged; //unsunscribe, just in case
            CalculateSum();
        }

        public void Delete(Table t)
        {
            Tables.Remove(t);
            t.OnSumChanged -= Table_OnSumChanged; //unsunscribe, just in case
            CalculateSum();
        }

        public IReadOnlyCollection<Table> ReadAll()
        {
            return Tables.AsReadOnly();
        }

        public void Update(Table table) { }

        public int Count => Tables.Count;

        #endregion
    }
}

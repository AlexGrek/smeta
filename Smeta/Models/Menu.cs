﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smeta.Data;
using Windows.ApplicationModel.Resources;
using Windows.Storage;

namespace Smeta.Models
{
    public class Menu
    {
        public StorageFile CurrentFile;
        public string Path { get => CurrentFile?.Path; }

        public string FileName
        {
            get
            {
                if (Path != null)
                {
                    return System.IO.Path.GetFileNameWithoutExtension(Path);
                }
                else
                {
                    var loader = new ResourceLoader();
                    var resourceString = loader.GetString("NewFileName");
                    return resourceString;
                }
            }
        }


        public async Task<bool> ExportToJson(Estimate estimate)
        {
            var savePicker = new Windows.Storage.Pickers.FileSavePicker
            {
                SuggestedStartLocation =
                    Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary
            };

            savePicker.FileTypeChoices.Add("JSON", new List<string>() { ".json" });
            savePicker.SuggestedFileName = FileName;

            var picked = await savePicker.PickSaveFileAsync();
            if (picked != null)
            {
                var tool = new JsonTool(picked);
                await Task.Run(() => tool.UpdateByNameAsync(picked.Path, estimate));

                return true;
            }
            return false;
        }

        public async Task<bool> ExportToCSV(Estimate estimate)
        {
            var savePicker = new Windows.Storage.Pickers.FileSavePicker
            {
                SuggestedStartLocation =
                    Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary
            };

            savePicker.FileTypeChoices.Add("CSV", new List<string>() { ".csv" });
            savePicker.SuggestedFileName = FileName;

            var picked = await savePicker.PickSaveFileAsync();
            if (picked != null)
            {
                var tool = new CSVTool(picked);
                await Task.Run(() => tool.UpdateByNameAsync(picked.Path, estimate));

                return true;
            }
            return false;
        }

        public async Task<bool> ExportToXML(Estimate estimate)
        {
            var savePicker = new Windows.Storage.Pickers.FileSavePicker
            {
                SuggestedStartLocation =
                    Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary
            };

            savePicker.FileTypeChoices.Add("XML", new List<string>() { ".xml" });
            savePicker.SuggestedFileName = FileName;

            var picked = await savePicker.PickSaveFileAsync();
            if (picked != null)
            {
                var tool = new XMLTool(picked);
                await Task.Run(() => tool.UpdateByNameAsync(picked.Path, estimate));

                return true;
            }
            return false;
        }

        public async Task<bool> Save(Estimate estimate)
        {
            if (CurrentFile == null)
            {
                return await SaveAs(estimate);
            }
            else
            {
                var tool = new XMLTool(CurrentFile);
                await Task.Run(() => tool.UpdateByNameAsync(CurrentFile.Path, estimate));
                return true;
            }
        }

        public async Task<bool> SaveAs(Estimate estimate)
        {
            var savePicker = new Windows.Storage.Pickers.FileSavePicker
            {
                SuggestedStartLocation =
                    Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary
            };

            savePicker.FileTypeChoices.Add("XML", new List<string>() { ".xml" });
            savePicker.SuggestedFileName = FileName;

            var picked = await savePicker.PickSaveFileAsync();
            if (picked != null)
            {
                CurrentFile = picked;

                var tool = new XMLTool(picked);
                await Task.Run(() => tool.UpdateByNameAsync(picked.Path, estimate));
                return true;
            }
            return false;
        }

        public async Task<Estimate> Load()
        {
            var loadPicker = new Windows.Storage.Pickers.FileOpenPicker
            {
                SuggestedStartLocation =
                    Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary
            };
            loadPicker.FileTypeFilter.Add(".xml");
            var picked = await loadPicker.PickSingleFileAsync();
            if (picked == null)
                return null;
            CurrentFile = picked;
            var tool = new XMLTool(picked);
            var est = await Task.Run(() => tool.LoadFrom(picked));
            return est;
        }
    }
}

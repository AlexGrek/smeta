﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smeta.Models
{
    public class Table
    {
        ObservableCollection<TableRow> _rows = new ObservableCollection<TableRow>();
        decimal _sum;
        string _name = "";

        public static Table CreateInitial()
        {
            var t = new Table();
            t.Name = "Table";
            t.Add(TableRow.CreateInitial());
            return t;
        }



        /// <summary>
        /// Triggers when sum is recalculated
        /// </summary>
        public event SumChanged OnSumChanged;

        public string Name { get => _name; set => _name = value; }
        public decimal Sum { get => _sum; private set => _sum = value; }
        public ObservableCollection<TableRow> Rows { get => _rows; private set => _rows = value; }

        public Table(string name)
        {
            Sum = 0;
            Name = name;
            SignForCollectionUpdate();
        }

        public Table() : this("")
        {
        }

        public Table(IEnumerable<TableRow> rows, string name = "")
        {
            Sum = 0;
            Name = name;
            foreach (var row in rows)
            {
                Rows.Add(row);
                row.OnSumChanged += Row_OnSumChanged;
            }
            SignForCollectionUpdate();
        }

        /// <summary>
        /// Implementation of row.SumChanged
        /// </summary>
        /// <param name="news">new sum</param>
        /// <param name="olds">old sum</param>
        private void Row_OnSumChanged(decimal news, decimal olds)
        {
            CalculateSum();
        }

        /// <summary>
        /// Signs for ObservableCollection's event CollectionChanged
        /// </summary>
        private void SignForCollectionUpdate()
        {
            Rows.CollectionChanged += Rows_CollectionChanged;
        }


        /// <summary>
        /// Implementation of CollectionChanged
        /// </summary>
        /// <param name="sender">sender - unused</param>
        /// <param name="e">parameter - unused</param>
        private void Rows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RecalculateNumbers();
            CalculateSum();
        }


        /// <summary>
        /// Recalculates row numbers for all the table
        /// </summary>
        public void RecalculateNumbers()
        {
            int n = 0;
            foreach (var row in Rows)
            {
                if (row.CustomID)
                {
                    // inherit row's number
                    n = row.ID;
                }
                else
                {
                    // use default numbering
                    n += 1;
                    row.ID = n;
                }
            }
        }

        /// <summary>
        /// RECalculates sum for table, triggers OnSumChanged always.
        /// </summary>
        /// <returns>New sum</returns>
        public decimal CalculateSum()
        {
            var newsum = Rows.Sum(row => row.Sum);
            var oldsum = Sum;
            Sum = newsum;
            OnSumChanged?.Invoke(newsum, oldsum);
            return newsum;
        }

        #region CRUD

        public void AddEmpty()
        {
            var row = new TableRow();
            row.OnSumChanged += Row_OnSumChanged;
        }

        public void Add(TableRow row)
        {
            row.OnSumChanged += Row_OnSumChanged;
            Rows.Add(row);
        }

        public void AddRange(IEnumerable<TableRow> rows)
        {
            foreach (var row in rows)
            {
                Add(row);
            }
        }

        public void Insert(int a)
        {
            var row = new TableRow();
            row.OnSumChanged += Row_OnSumChanged;
            Rows.Insert(a, row);
        }

        public void Insert(int a, TableRow row)
        {
            row.OnSumChanged += Row_OnSumChanged;
            Rows.Insert(a, row);
        }

        public void Delete(int i)
        {
            var row = Rows[i];
            Rows.RemoveAt(i);
            row.OnSumChanged -= Row_OnSumChanged; //unsunscribe, just in case
        }

        public void Delete(TableRow row)
        {
            Rows.Remove(row);
            row.OnSumChanged -= Row_OnSumChanged; //unsubscribe, just in case
        }

        public void Update(TableRow row)
        {

        }

        #endregion
    }
}

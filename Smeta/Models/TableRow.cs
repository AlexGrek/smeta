﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smeta.Models
{
    public delegate void SumChanged(decimal news, decimal olds);

    public class TableRow
    {
        /// <summary>
        /// Triggers when sum is changed
        /// </summary>
        public event SumChanged OnSumChanged;

        public static TableRow CreateInitial()
        {
            return new TableRow("", "", 0, 0);
        }

        public string Name;
        public string Units;
        decimal _count;
        decimal _price;
        decimal _sum;
        int _id;
        bool _customID;

        public decimal Sum { get => _sum; private set => _sum = value; }
        public decimal Price { get => _price;
            set
            {
                _price = value;
                RecalculateSum();
            }
        }
        public decimal Count { get => _count;
            set
            {
                _count = value;
                RecalculateSum();
            }
        }

        public int ID { get => _id; set => _id = value; }
        public bool CustomID { get => _customID; set => _customID = value; }

        public TableRow(string name = "", string units = "", decimal count = 0, decimal price = 0)
        {
            Name = name;
            Units = units;
            Count = count;
            Price = price;
        }

        public TableRow()
        {
            Name = "";
            Units = "";
            Count = 0;
            Price = 0;
        }

        /// <summary>
        /// Calculate new sum and return if sum is changed.
        /// Triggers OnSumChanged if it was changed.
        /// </summary>
        /// <returns>sum changed</returns>
        public bool RecalculateSum()
        {
            var old = Sum;
            Sum = Count * Price;
            //Debug.WriteLine($"{Count} * {Price}$ = {Sum}$");
            if (old != Sum)
            {
                if (OnSumChanged != null)
                    OnSumChanged.Invoke(Sum, old);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a copy of the row
        /// </summary>
        /// <returns>copy of the row, not connected with original row</returns>
        public TableRow Clone()
        {
            var row = new TableRow(Name, Units, Count, Price);
            return row;
        }
    }
}

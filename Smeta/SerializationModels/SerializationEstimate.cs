﻿using Smeta.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smeta.SerializationModels
{
    public class SerializationEstimate
    {
        public static SerializationEstimate Create(Estimate estimate)
        {
            var tabs = new List<SerializationTable>();
            foreach (var item in estimate.Tables)
            {
                tabs.Add(SerializationTable.Create(item));
            }

            var serializable = new SerializationEstimate()
            {
                Sum = estimate.Sum,
                Name = estimate.Name,
                Tables = tabs
            };
            return serializable;
        }

        public List<SerializationTable> Tables;
        public decimal Sum;
        public string Name;

        public Estimate Decode()
        {
            var est = new Estimate() { Name = Name };
            foreach (var table in Tables)
            {
                est.Add(table.Decode());
            }
            return est;
        }
    }
}

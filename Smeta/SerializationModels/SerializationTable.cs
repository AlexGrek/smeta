﻿using Smeta.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smeta.SerializationModels
{
    public class SerializationTable
    {
        public static SerializationTable Create(Table table)
        {
            var rws = new List<SerializationTableRow>();
            foreach (var item in table.Rows)
            {
                rws.Add(SerializationTableRow.Create(item));
            }

            var serializable = new SerializationTable()
            {
                Sum = table.Sum,
                Name = table.Name,
                Rows = rws
            };
            return serializable;
        }

        public List<SerializationTableRow> Rows;
        public decimal Sum;
        public string Name;

        public Table Decode()
        {
            var tab = new Table()
            {
                Name = Name
            };
            foreach (var row in Rows)
            {
                tab.Add(row.Decode());
            }
            return tab;
        }
    }
}

﻿using Smeta.Models;

namespace Smeta.SerializationModels
{
    public class SerializationTableRow
    {
        public static SerializationTableRow Create(TableRow r)
        {
            var serializable = new SerializationTableRow()
            {
                Sum = r.Sum,
                Name = r.Name,
                ID = r.ID,
                Units = r.Units,
                Count = r.Count,
                Price = r.Price
            };
            return serializable;
        }

        public decimal Sum;
        public string Name;
        public decimal Price;
        public decimal Count;
        public string Units;
        public int ID;

        public TableRow Decode()
        {
            var row = new TableRow()
            {
                Name = Name,
                Price = Price,
                Count = Count,
                Units = Units,
                ID = ID
            };
            return row;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Smeta.Models;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml;

namespace Smeta.ViewModels
{
    public class EstimateViewModel: NotificationBase
    {
        Models.Estimate _est;
        ResourceLoader _resourceLoader = ResourceLoader.GetForCurrentView();

        public void UpdateEstimate(Estimate init)
        {
            Est = init;
            _tableViewModels.Clear();
            // Load the database
            foreach (var item in Est.ReadAll())
            {
                var np = new TableViewModel(item);
                np.PropertyChanged += Np_PropertyChanged;
                _tableViewModels.Add(np);
            }
            Est.OnSumChanged += Table_OnSumChanged;
            RaisePropertyChanged("Sum");
            RaisePropertyChanged("Name");
            RaisePropertyChanged("DeleteCommand");
        }

        public EstimateViewModel(): this("")
        {
        }

        public EstimateViewModel(String name = "", Estimate init = null)
        {

            _SelectedIndex = -1;

            if (init != null)
            {
                Est = init;
            }
            else
            {
                Est = new Estimate(name);
            }

            // Load the database
            foreach (var item in Est.ReadAll())
            {
                var np = new TableViewModel(item);
                np.PropertyChanged += Np_PropertyChanged;
                _tableViewModels.Add(np);
            }

            Est.OnSumChanged += Table_OnSumChanged;
        }

        public EstimateViewModel(Estimate init) : this(init.Name, init) { }

        private void Table_OnSumChanged(decimal news, decimal olds)
        {
            RaisePropertyChanged("Sum");
        }

        private void Np_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //Est.Update((TableViewModel)sender);
        }

        ObservableCollection<TableViewModel> _tableViewModels = new ObservableCollection<TableViewModel>();
        public ObservableCollection<TableViewModel> TableVMs
        {
            get { return _tableViewModels; }
            set { SetProperty(ref _tableViewModels, value); }
        }

        public String Name
        {
            get { return _est.Name; }
            set
            {
                _est.Name = value;
                RaisePropertyChanged("Name");
            }
        }

        private Visibility _nameEditVisibility = Visibility.Collapsed;
        public Visibility NameEditVisibility
        {
            get { return _nameEditVisibility; }
            set
            {
                _nameEditVisibility = value;
                RaisePropertyChanged("NameEditVisibility");
            }
        }

        public decimal Sum
        {
            get { return _est.Sum; }
        }

        int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set { if (SetProperty(ref _SelectedIndex, value)) { RaisePropertyChanged(nameof(SelectedPerson)); } }
        }

        public TableViewModel SelectedPerson
        {
            get { return (_SelectedIndex >= 0) ? _tableViewModels[_SelectedIndex] : null; }
        }

        public Estimate Est { get => _est; set => _est = value; }

        public void Add()
        {
            var newTableName = _resourceLoader.GetString("NewTable") + Est.Count.ToString();
            var table = new Table(newTableName);
            var vm = new TableViewModel(table);
            vm.PropertyChanged += Person_OnNotifyPropertyChanged;
            TableVMs.Add(vm);
            Est.Add(table);
            SelectedIndex = TableVMs.IndexOf(vm);
        }

        public ICommand DeleteCommand => new RelayCommand(x => Delete(x as TableViewModel));
        public void Delete(TableViewModel vm)
        {
            Est.Delete(vm.Table);
            _tableViewModels.Remove(vm);
            Debug.WriteLine("delete called!");
        }

        public ICommand ShowOrHideEditboxCommand => new RelayCommand(x => ShowOrHideEditbox());

        public void ShowOrHideEditbox()
        {
            if (NameEditVisibility == Visibility.Collapsed)
                NameEditVisibility = Visibility.Visible;
            else
                NameEditVisibility = Visibility.Collapsed;
        }

        void Person_OnNotifyPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            Est.Update(((TableViewModel)sender).Table);
        }
    }
}

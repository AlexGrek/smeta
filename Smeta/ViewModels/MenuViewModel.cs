﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;

namespace Smeta.ViewModels
{
    public class MenuViewModel: NotificationBase<Models.Menu>
    {
        private EstimateViewModel _evm = new EstimateViewModel(Models.Estimate.CreateInitial());
        public EstimateViewModel CurrentEstimate { get { return _evm; } set {
                _evm = value;
                RaisePropertyChanged("CurrentEstimate");
            } }
            


        public ICommand ExportToJsonCommand => new RelayCommand(x => ExportToJson(x as EstimateViewModel));

        private async void ExportToJson(EstimateViewModel evm)
        {
            await This.ExportToJson(evm.Est);
        }

        public ICommand ExportToCSVCommand => new RelayCommand(x => ExportToCSV(x as EstimateViewModel));

        private async void ExportToCSV(EstimateViewModel evm)
        {
            await This.ExportToCSV(evm.Est);
        }

        public ICommand LoadCommand => new RelayCommand(x => Load());
        private async void Load()
        {
            var loaded = await This.Load();
            if (loaded != null)
            {
                CurrentEstimate = new EstimateViewModel(loaded);
            }
        }

        public ICommand SaveCommand => new RelayCommand(x => Save(x as EstimateViewModel));
        private async void Save(EstimateViewModel evm)
        {
            await This.Save(evm.Est);
        }

        public ICommand SaveAsCommand => new RelayCommand(x => SaveAs(x as EstimateViewModel));
        private async void SaveAs(EstimateViewModel evm)
        {
            await This.SaveAs(evm.Est);
        }

        public ICommand ExportToXMLCommand => new RelayCommand(x => ExportToXML(x as EstimateViewModel));

        private async void ExportToXML(EstimateViewModel evm)
        {
            await This.ExportToXML(evm.Est);
        }

        public ICommand ExitAppCommand => new RelayCommand(x => ExitApplication());

        private void ExitApplication()
        {
            Application.Current.Exit();
        }
    }
}

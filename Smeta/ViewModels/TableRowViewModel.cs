﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smeta.ViewModels
{
    public class TableRowViewModel: NotificationBase<Models.TableRow>
    {
        public TableRowViewModel(Models.TableRow row = null) : base(row)
        {
            This.OnSumChanged += This_OnSumChanged;
        }

        private void This_OnSumChanged(decimal news, decimal olds)
        {
            RaisePropertyChanged("Sum");
        }

        public String Name
        {
            get { return This.Name; }
            set { SetProperty(This.Name, value, () => This.Name = value); }
        }

        public String Units
        {
            get { return This.Units; }
            set { SetProperty(This.Units, value, () => This.Units = value); }
        }

        public decimal Price
        {
            get { return This.Price; }
            set { SetProperty(This.Price, value, () => This.Price = value); }
        }

        public decimal Count
        {
            get { return This.Count; }
            set { SetProperty(This.Count, value, () => This.Count = value); }
        }

        public decimal Sum
        {
            get { return This.Sum; }
        }

        public decimal ID
        {
            get { return This.ID; }
        }

        public void NumberUpdated()
        {
            RaisePropertyChanged("ID");
        }

        public Models.TableRow Item => this.This;
        public TableRowViewModel VM => this;
    }
}

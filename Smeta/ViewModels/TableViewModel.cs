﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Smeta.Models;

namespace Smeta.ViewModels
{
    public class TableViewModel: NotificationBase
    {
        Models.Table _table;

        public TableViewModel(String name, Table init = null)
        {
            
            _SelectedIndex = -1;

            if (init != null)
            {
                Table = init;
            }
            else
            {
                Table = new Table(name);
            }

            // Load the database
            foreach (var row in Table.Rows)
            {
                var rowVM = new TableRowViewModel(row);
                rowVM.PropertyChanged += Np_PropertyChanged; ;
                _rows.Add(rowVM);
            }

            Table.OnSumChanged += Table_OnSumChanged;
        }

        public void UpdateRowNumbers()
        {
            foreach (var row in _rows)
            {
                row.NumberUpdated();
            }
        }

        public TableViewModel(Table init) : this(init.Name, init) { }

        private void Table_OnSumChanged(decimal news, decimal olds)
        {
            RaisePropertyChanged("Sum");
        }

        private void Np_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Table.Update((TableRowViewModel)sender);
        }

        ObservableCollection<TableRowViewModel> _rows = new ObservableCollection<TableRowViewModel>();
        public ObservableCollection<TableRowViewModel> Rows
        {
            get { return _rows; }
            set { SetProperty(ref _rows, value); }
        }

        public String Name
        {
            get { return Table.Name; }
            set { Table.Name = value; RaisePropertyChanged("Name"); }
        }

        public decimal Sum
        {
            get { return Table.Sum; }
        }

        int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set { if (SetProperty(ref _SelectedIndex, value)) { RaisePropertyChanged(nameof(SelectedPerson)); } }
        }

        public TableRowViewModel SelectedPerson
        {
            get { return (_SelectedIndex >= 0) ? _rows[_SelectedIndex] : null; }
        }

        public Table Table { get => _table; private set => _table = value; }

        public void Add()
        {
            var row = new TableRowViewModel();
            row.PropertyChanged += Person_OnNotifyPropertyChanged;
            Rows.Add(row);
            Table.Add(row);
            SelectedIndex = Rows.IndexOf(row);
            UpdateRowNumbers();
        }

        public ICommand DeleteCommand => new RelayCommand(x => Delete(x as TableRowViewModel));

        public void Delete(TableRowViewModel row)
        {
            Rows.Remove(row);
            _table.Delete(row.Item);
            UpdateRowNumbers();
        }

        public ICommand DuplicateCommand => new RelayCommand(x => Duplicate(x as TableRowViewModel));

        public void Duplicate(TableRowViewModel row)
        {
            var rowPosition = Rows.IndexOf(row);
            var duplicate = row.Item.Clone();
            var duplicateVM = new TableRowViewModel(duplicate);
            Rows.Insert(rowPosition, duplicateVM);
            _table.Insert(rowPosition, duplicate);
            duplicateVM.PropertyChanged += Person_OnNotifyPropertyChanged;
            UpdateRowNumbers();
        }

        void Person_OnNotifyPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            Table.Update(((TableRowViewModel)sender).Item);
        }

        public TableViewModel VM => this;

        public override string ToString()
        {
            return ":" + this.Name;
        }
    }
}

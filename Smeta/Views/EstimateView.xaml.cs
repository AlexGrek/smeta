﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Smeta.ViewModels;
using System.Diagnostics;
using Smeta.Models;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Smeta.Views
{
    public sealed partial class EstimateView : UserControl
    {
        public EstimateView()
        {
            this.InitializeComponent();
        }

        internal EstimateViewModel Model => (EstimateViewModel)Resources["ViewModel"];

        private void EditTableNameButton_Click(object sender, RoutedEventArgs e)
        {
            FlyoutBase.ShowAttachedFlyout((FrameworkElement)sender);
        }

        private void DeleteButton1_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            //Debug.Fail("ERROR!");
        }

        private void Grid_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            Debug.WriteLine("DataContext changed!");
        }

        // NORMAL implementation

        public static readonly DependencyProperty EstimateProperty = DependencyProperty.Register(
    "Project", typeof(Estimate), typeof(EstimateView),
    new PropertyMetadata(Estimate.CreateInitial(), OnProjectChanged));

        public Estimate CurrentEstimate
        {
            get { return (Estimate)GetValue(EstimateProperty); }
            set { SetValue(EstimateProperty, value); }
        }

        private static void OnProjectChanged(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((EstimateView)obj).Model.UpdateEstimate((Estimate)args.NewValue);
        }

    }
}

﻿using Smeta.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Smeta.Views
{
    public sealed partial class TableView : UserControl
    {
        public TableView()
        {
            this.InitializeComponent();
        }

        public TableViewModel Table
        {
            get
            {
                return (TableViewModel)this.GetValue(TableProperty);
            }
            set
            {
                //_estimate.UpdateEstimate(value.Est);
                this.SetValue(TableProperty, value);
            }
        }

        public static readonly DependencyProperty TableProperty = DependencyProperty.Register(
            "Table", typeof(TableViewModel), typeof(TableView), new PropertyMetadata(new TableViewModel(Models.Table.CreateInitial())));
    }
}
